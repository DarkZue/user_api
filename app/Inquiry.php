<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inquiry extends Model
{
    protected $fillable = [
        'name', 'email', 'url', 'body'
    ];

    protected $hidden = [
        'deleted_at', 'updated_at', 'id'
    ];

    protected $table = 'inquiries';
}
